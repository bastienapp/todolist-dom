// DOM
// console.log(document);

// récupérer le bouton
// const buttonSend = document.querySelector('button');
const buttonSend = document.getElementById('button__send');
console.log(buttonSend);

// on tableau d'éléments qui ont une classe
// document.getElementsByClassName()
// document.querySelectorAll()

// récupérer l'action du clic sur le bouton
/*
buttonSend.onclick = function() {
  alert("ça marche !");
}
*/
buttonSend.addEventListener("click", function(event) {
  // évite l'envoi du formulaire et le rechargement de la page
  event.preventDefault();

  // récupérer le texte du champ "Nom"
  const userNameElement = document.getElementById("input__name");
  // si le nom est vide, afficher "Merci de renseigner votre nom"
  const userNameValue = userNameElement.value;

  // récupérer la div qui va contenir le texte
  const messageElement = document.getElementById('div__message');

  // remplacer le contenu de la div par "bonjour " + prénom
  if (userNameValue === "") {
    // innerHTML : il peut y avoir des faille, ex: injection de javascript
    messageElement.textContent = "Merci de renseigner votre nom";
  } else {
    // sinon afficher Bonjour + le nom
    messageElement.textContent = "Bonjour " + userNameValue;
  }

});